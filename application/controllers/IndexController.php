<?php

class IndexController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $form = new Application_Form_Password();
        $this->view->form = $form;
    }

    public function generateRandomAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        if ($this->_request->isXmlHttpRequest()) {
            $creator = new \Utils\PasswordCreator();
            $creator->createPassword();
            $this->_helper->json($creator->getPassword()->toArray());
        }
    }

    public function validatePasswordAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        if ($this->getRequest()->isXmlHttpRequest() && $this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();
            $form = new Application_Form_Password();
            if ($form->isValid($params)) {
                $this->_helper->json(['success' => true]);
            } else {
                $this->_helper->json(['success' => false, 'messages' => $form->getMessages()]);
            }
        }
    }
}
