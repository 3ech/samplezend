<?php

class Application_Form_Password extends Zend_Form
{

    public function init()
    {
        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage("wypełnij pole", Zend_Validate_NotEmpty::IS_EMPTY);

        $this->addElement('text', 'password', array(
            'label' => 'Hasło',
            'required' => true,
            'validators' => [
                    [new Validate\Password()],
                    [$notEmpty]
            ]
        ));
        $this->addElement('button', 'validatePassword', array(
            'label' => 'Sprawdź poprawność hasła',
        ));
        $this->addElement('button', 'generatePassword', array(
            'label' => 'Generuj hasło',
        ));
    }

    /**
     * Walidacja
     * @param array $data parametry przeslane POSTem
     */
    public function isValid($data)
    {
        return parent::isValid($data);
    }
}
