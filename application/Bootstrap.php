<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    public function _initRoutes()
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();

        $router->addRoute('createRandomPassword', new Zend_Controller_Router_Route('password/generate', array('module' => 'index', 'controller' => 'index', 'action' => 'generate-random')));
        $router->addRoute('validatePassword', new Zend_Controller_Router_Route('password/validate', array('module' => 'index', 'controller' => 'index', 'action' => 'validate-password')));
    }
}
