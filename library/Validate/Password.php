<?php

namespace Validate;

/**
 * Założenia:
 * Minimum 8 znaków
 * Maksimum 20 znaków
 * Minimum 1 mała i duża litera
 * Minimum 1 cyfra
 * Minimum 1 znak specjalny
 */
class Password extends \Zend_Validate_Abstract
{
    const MIN_CHAR_COUNT = 8;
    const MAX_CHAR_COUNT = 20;
    const MIN_UPPERCASE_CHAR_COUNT = 1;
    const MIN_LOWERCASE_CHAR_COUNT = 1;
    const MIN_SPECIAL_CHAR_COUNT = 1;
    const MIN_DIGIT_COUNT = 1;
    const IS_EMPTY = 'errorEmpty';
    const SPECIAL_CHARS = '~!@#$%^&*()_+=-`[]\\|}{:"\';:"?>./,<';
    const LOWERCASE_LETTERS = 'qwertyuioplkjhgfdsazxcvbńźżćłśąęónm';
    const UPPERCASE_LETTERS = 'QWERTYUIOPASDFGHJKLZXCVBNMĘÓŁŚĄŹŻŃĆ';
    const DIGITS = '0123456789';
    const ERROR_MIN_CHAR_COUNT = 'ERROR_MIN_CHAR_COUNT';
    const ERROR_MAX_CHAR_COUNT = 'MAX_CHAR_COUNT';
    const ERROR_MIN_UPPERCASE_CHAR_COUNT = 'MIN_UPPERCASE_CHAR_COUNT';
    const ERROR_MIN_LOWERCASE_CHAR_COUNT = 'MIN_LOWERCASE_CHAR_COUNT';
    const ERROR_MIN_SPECIAL_CHAR_COUNT = 'MIN_SPECIAL_CHAR_COUNT';
    const ERROR_MIN_DIGIT_COUNT = 'MIN_DIGIT_COUNT';
    protected static $_filter = null;
    protected $_messageTemplates;

    public function __construct()
    {
        $this->_messageTemplates = array(
            self::IS_EMPTY => 'Hasło nie może być puste',
            self::ERROR_MIN_CHAR_COUNT => 'Hasło musi mieć więcej niż ' . self::MIN_CHAR_COUNT . ' znaków',
            self::ERROR_MAX_CHAR_COUNT => 'Hasło musi mieć mniej niż ' . self::MAX_CHAR_COUNT . ' znaków',
            self::ERROR_MIN_UPPERCASE_CHAR_COUNT => 'Liczba wielkich liter musi być większa niż ' . self::MIN_UPPERCASE_CHAR_COUNT,
            self::ERROR_MIN_LOWERCASE_CHAR_COUNT => 'Liczba małych liter musi być większa niż ' . self::MIN_LOWERCASE_CHAR_COUNT,
            self::ERROR_MIN_SPECIAL_CHAR_COUNT => 'Liczba znaków specjalnych musi być większa niż ' . self::MIN_SPECIAL_CHAR_COUNT,
            self::ERROR_MIN_DIGIT_COUNT => 'Liczba cyfer musi być większa niż ' . self::MIN_DIGIT_COUNT
        );
    }

    public function isValid($value, $context = null)
    {

        $this->_setValue($value);

        if (empty($value)) {
            $this->_error(self::IS_EMPTY);
            return false;
        }

        if ($this->getCharactersCount($value, self::SPECIAL_CHARS) < self::MIN_SPECIAL_CHAR_COUNT) {
            $this->_error(self::ERROR_MIN_SPECIAL_CHAR_COUNT);
            return false;
        }

        if ($this->getCharactersCount($value, self::LOWERCASE_LETTERS) < self::MIN_LOWERCASE_CHAR_COUNT) {
            $this->_error(self::ERROR_MIN_LOWERCASE_CHAR_COUNT);
            return false;
        }
        if ($this->getCharactersCount($value, self::UPPERCASE_LETTERS) < self::MIN_UPPERCASE_CHAR_COUNT) {
            $this->_error(self::ERROR_MIN_UPPERCASE_CHAR_COUNT);
            return false;
        }
        if ($this->getCharactersCount($value, self::DIGITS) < self::MIN_DIGIT_COUNT) {
            $this->_error(self::ERROR_MIN_DIGIT_COUNT);
            return false;
        }
        $charCount = $this->_checkCharCount($value);
        if (!$charCount) {
            return false;
        }


        return true;
    }

    /**
     * Zwraca liczbę znaków z $characters w $value 
     * @param type $value
     * @param type $characters
     * @return int
     */
    private function getCharactersCount($value, $characters)
    {
        $characters = str_split($characters);
        $charactersCount = 0;
        $value = str_split($value);
        foreach ($value as $valueChar) {
            if (in_array($valueChar, $characters)) {
                $charactersCount++;
            }
        }
        return $charactersCount;
    }

    private function _checkCharCount($value)
    {
        $valueLength = mb_strlen($value, 'UTF-8');
        if ($valueLength < self::MIN_CHAR_COUNT) {
            $this->_error(self::ERROR_MIN_CHAR_COUNT);
            return false;
        } elseif ($valueLength > self::MAX_CHAR_COUNT) {
            $this->_error(self::ERROR_MAX_CHAR_COUNT);
            return false;
        }
        return true;
    }
}

?>
