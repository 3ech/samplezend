<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Utils;

/**
 * Description of Password
 *
 * @author mczech
 */
class Password
{
    private $hash;
    private $lastPasswordChangeDate;
    private $plainPassword;

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function getLastPasswordChangeDate()
    {
        return $this->lastPasswordChangeDate;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    public function setLastPasswordChangeDate($lastPasswordChangeDate)
    {
        $this->lastPasswordChangeDate = $lastPasswordChangeDate;
        return $this;
    }
}
