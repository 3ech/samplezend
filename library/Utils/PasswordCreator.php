<?php

namespace Utils;

use LogicException;
use Validate\Password as PasswordValidator;

class PasswordCreator
{
    /**
     *
     * @var Password
     */
    private $password;
    private $plainPassword;
    const DEFAULT_WORK_FACTOR = 8;

    public function __construct($password = NULL)
    {
        $this->password = new Password();
        $this->_setPlainPassword($password);
    }

    /**
     * Przypisywanie hasła podanego przy rejestracji (lub nie podanego) w UserService::register()
     * W przypadku nie podania, generuje się w następnych krokach losowe hasło
     * @param string $password
     * @throws LogicException
     */
    private function _setPlainPassword($password = NULL)
    {
        if ($password === NULL) {
            $password = $this->_generateRandomPassword();
        }
        $passwordValidator = new PasswordValidator();
        if (!$passwordValidator->isValid($password)) {
            $errorMessage = $passwordValidator->getMessageTemplates()[$passwordValidator->getErrors()[0]];
            throw new LogicException('Password doesn\'t fit requirements: ' . $errorMessage);
        }
        $this->plainPassword = $password;
        $this->password->setPlainPassword($password);
    }

    /**
     * Generowanie losowego hasła
     * @return string
     */
    private function _generateRandomPassword()
    {
        $digits = str_split(PasswordValidator::DIGITS);
        $specialChars = str_split(PasswordValidator::SPECIAL_CHARS);
        $lowercaseChars = $this->_utf8Split(PasswordValidator::LOWERCASE_LETTERS);
        $uppercaseChars = $this->_utf8Split(PasswordValidator::UPPERCASE_LETTERS);
        $randomPasswordArray = array();
        for ($i = 0; $i < 4; $i++) {
            $randomPasswordArray[] = $digits[rand(0, sizeof($digits) - 1)];
            $randomPasswordArray[] = $specialChars[rand(0, sizeof($specialChars) - 1)];
            $randomPasswordArray[] = $lowercaseChars[rand(0, sizeof($lowercaseChars) - 1)];
            $randomPasswordArray[] = $uppercaseChars[rand(0, sizeof($uppercaseChars) - 1)];
        }
        shuffle($randomPasswordArray);
        $randomPassword = implode('', $randomPasswordArray);
        return $randomPassword;
    }

    /**
     * Rozdziela string zapisany w formacie utf8 na array
     * Dla polskich znaków nie wystarczy str_split
     * @param string $str
     * @param int $len
     * @return array
     */
    private function _utf8Split($str, $len = 1)
    {
        $arr = array();
        $strLen = mb_strlen($str, 'UTF-8');
        for ($i = 0; $i < $strLen; $i++) {
            $arr[] = mb_substr($str, $i, $len, 'UTF-8');
        }
        return $arr;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * 
     * @return Password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Tworzenie hasła
     * @return boolean
     */
    public function createPassword()
    {
        $hash = $this->_createRandomHash($this->plainPassword);
        $this->password->setHash($hash);
        $this->password->setLastPasswordChangeDate('0000-00-00 00:00:00');
        return true;
    }

    /**
     * Generowanie unikalnego hash
     * @return string
     */
    private function _createRandomHash($password, $work_factor = 0)
    {
        if (version_compare(PHP_VERSION, '5.3') < 0)
            throw new Exception('Bcrypt requires PHP 5.3 or above');

        if (!function_exists('openssl_random_pseudo_bytes')) {
            throw new Exception('Bcrypt requires openssl PHP extension');
        }

        if ($work_factor < 4 || $work_factor > 31)
            $work_factor = self::DEFAULT_WORK_FACTOR;
        $salt = '$2a$' . str_pad($work_factor, 2, '0', STR_PAD_LEFT) . '$' .
            substr(
                strtr(base64_encode(openssl_random_pseudo_bytes(16)), '+', '.'), 0, 22
            )
        ;
        return crypt($password, $salt);
    }
}
